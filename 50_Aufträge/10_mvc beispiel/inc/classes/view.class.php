<?php
/*
 * Autor: Tobias Peter
 * Datum: 20.03.2018
 * Projekt: MVC vorstellen
 * Dateiname: views.class.php
 * Zweck: Verwaltet Views
 */

class View {
	private $name = null;
	private $innerhtml = null;
	
	public function __construct($name, $preload=true, $predisplay=true) {
		$this->name = $name;
		if($preload) {
			$this->load();
		}
		if($predisplay) {
			$this->display();
		}
	}
	
	public function load() {
		$this->innerhtml = file_get_contents(__DIR__."/../../".VIEWS_PATH.$this->name.".html");
	}
	
	public function display() {
		echo $this->innerhtml;
	}
}
?>