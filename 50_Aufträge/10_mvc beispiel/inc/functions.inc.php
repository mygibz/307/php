<?php
/*
 * Autor: Tobias Peter
 * Datum: 20.03.2018
 * Projekt: MVC vorstellen
 * Dateiname: functions.inc.php
 * Zweck: Beinhaltet Grundfunktionen
 */

// Klassen automatisch laden
function __autoload($class) {
	$class_file = __DIR__."/classes/".ucfirst($class).".class.php";
	if(file_exists($class_file)) {
		include_once($class_file);
	} else {
		die("Klasse $class wurde nicht gefunden!" . __DIR__."/classes/".ucfirst($class).".class.php";);
	}
}

?>