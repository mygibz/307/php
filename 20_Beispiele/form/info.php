<html>
    <head>
        <title>PHP Demonstration</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body>
        <h1>Personeninformationen</h1>
        <div>
            <div>
                Name: <?php echo $_POST["lastName"]; ?>
            </div>
            <div>
                Vorname: <?php echo $_POST["firstName"]; ?>
            </div>
            <div>
                Adresse: <?php echo $_POST["street"] . " " . $_POST["city"]; ?>
            </div>
            <div>
            <?php
                $birthdate = new DateTime();
                $birthdate->setDate($_POST["year"], $_POST["month"], $_POST["day"]);
                $now = new DateTime();
                $age = $birthdate->diff($now);
                echo $age->format("Alter: %y");
            ?>
            </div>  
            <div>
                Beruf: <?php echo $_POST["job"]; ?>
            </div>
        </div>
    </body>
</html>
