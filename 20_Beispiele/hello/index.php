<!DOCTYPE html>
<html>
    <head>
        <title>En Titel</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="css/main.css" />
    </head>
    <body>
        <?php
            $date = new DateTime();
            echo $date->format("Y-m-d H:i:s");
        ?>
    </body>
</html>
