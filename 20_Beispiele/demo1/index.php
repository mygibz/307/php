<?php
$name = "philipp";
$lastname = "jost";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>welcome <?php echo $name ?></title>
  </head>
  <body>
    <h1>welcome to this demo <?php echo $name ?> <?php echo strtoupper($lastname) ?></h1>
    <p>
      <?php
      echo "a string with a variable which gets replaced: $name <br>";
      echo 'a string with a variable which doesn\'t get replaced: $name';
      ?>
    </p>
  </body>
</html>
