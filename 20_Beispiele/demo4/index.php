<?php
$array = array("array","of","strings");
$assocArray = array('name' => "phil",'lastname' => "yausst", 'favColor' => "lightcoral" );
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>demo4</title>
  </head>
  <body style="background-color:<?php echo $assocArray['favColor']?>">
    <h1>hello <?php echo $assocArray['name'] . " " . $assocArray['lastname'] ?></h1>
  </body>
</html>
