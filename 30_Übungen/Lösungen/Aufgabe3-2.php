<!DOCTYPE html>
<?php
$titel = 'PHP Farben Test';
$zufallswert = mt_rand(0, 255);
//$zufallswert = mt_rand(0, 16777215);
//16777215 steht für weiss und ist die letzte Farbe weshalb alles zwischen 0 und 16777215 die restlichen Farben sind.
$color = "#".dechex($zufallswert).dechex($zufallswert).dechex($zufallswert);
//$color = "#" . dechex($zufallswert);
 ?>
<html>
  <head>

    <meta charset="utf-8">
    <title><?php echo $titel ?></title>

  </head>
  <body bgcolor=<?php echo $color ?>>

  </body>
</html>
