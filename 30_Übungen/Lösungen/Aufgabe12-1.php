<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Zeit Berechnung</title>
    <?php
      //Variabeln Eingestellter Zeitpunkt
      $stunde = "24";
      $minute = "00";
      $sekunde = "00";
      $zeitpunkt = "$stunde:$minute:$sekunde";

      //Jetzige Zeit
      date_default_timezone_set("Europe/Berlin");
      $zeit = time();
      $uhrzeit = date("H:i:s", $zeit);


      $uhzeitstunde = date("H", $zeit);
      $uhrzeitMinute = date("i", $zeit);
      $uhrzeitSekunde = date("s", $zeit);

      //Berechnung Differenz
      $unterschied = ($stunde - $uhzeitstunde) * 3600;
      $unterschied += ($minute - $uhrzeitMinute) * 60;
      $unterschied += ($sekunde - $uhrzeitSekunde);

      //Ausgabe
      echo "Jetzt ist $uhrzeit<br>";
      echo "Eingestellter Zeitpunkt: $zeitpunkt<br>";
      echo "Unterschied: $unterschied Sekunden";

     ?>
  </head>
  <body>
  </body>
</html>
