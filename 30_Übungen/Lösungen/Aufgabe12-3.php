<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>

  </head>
  <body>
    <!--Formular-->
    <form action="Aufgabe12-3.php" method="get">
      Berechnung des Wochentages Ihres Geburtsdatums:<br>
      <input type="text" name="Eingabe"> Geburtstag (TT.MM.JJJJ)<br>
      <button type="submit">Submit</button>
    </form>


    <?php
    //Überprüfung ob get aktiviert
    if ($_SERVER["REQUEST_METHOD"] == "GET"){
      $datum = $_GET["Eingabe"];
      $wochentage = array('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag');

      list($tag, $monat, $jahr) = split('[.]', $datum);

      $datum = getdate(mktime(0,0,0, $monat, $tag, $jahr));
      $wochentag = $datum['wday'];

      //Ausgbabe
      echo $wochentage[$wochentag];
    }
     ?>
  </body>
</html>
