<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Datum & Uhrzeit</title>
    <?php

      //Variabeln
      date_default_timezone_set("Europe/Zurich");
      $zeit = time();
      $tag = date("j");
      $monat = date("m");
      $heute = date("j.m");
      $uhrzeit = date("H:i");
      $neujahr = "1.1";


      //Begrüssung nach Zeit
      setlocale(LC_TIME, "de_CH.utf8");
      if($uhrzeit >= 1 && $uhrzeit < 12){
        echo "Guten Morgen<br>";
      }
      elseif($uhrzeit >= 12 && $uhrzeit < 18){
        echo "Guten Tag<br>";
      }
      elseif($uhrzeit >= 18 && $uhrzeit < 2 ){
        echo "Guten Abend<br>";
      }

      //Berechnung Tage bis Neujahr
      $monat = $monat - 1;
      $tage = $tag;
      $tage += $monat * 30;
      $Jahr = "360" - $tage;

      //Ausgabe
      echo "Heute ist";
      echo strftime(" %A brah,");
      echo " der $tag. ";
      echo strftime(" %B brah, ");
      echo "$uhrzeit Uhr.<br>";
      echo "Bis zum Jahr 2018 geht es noch: $Jahr Tage"


     ?>
  </head>
  <body>

  </body>
</html>
