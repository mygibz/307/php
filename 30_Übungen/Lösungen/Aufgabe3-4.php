<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
    <?php
      function namensausgabe($x = "Ronald", $y = "13"){
        $satz = "Mein Name ist $x, ich bin $y Jahre alt";
        return $satz;
      }
     ?>

  </head>
  <body>
    <?php
      $satz2 = namensausgabe("Ronald", "30");
      echo $satz2;
     ?>
  </body>
</html>
